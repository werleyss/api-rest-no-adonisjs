# Adonis API applications

Este é o padrão para a criação de um servidor API no AdonisJs, vem pré-configurado com.

1. Bodyparser
2. Authentication
3. CORS
4. Lucid ORM
5. Migrations and seeds

## Setup

Use o comando adonis para instalar o blueprint

```bash
adonis new yardstick --api-only
```

ou clonar manualmente o repositório e depois executar npm install.


### Migrations

Execute o seguinte comando para executar migrações de inicialização.

```js
adonis migration:run
```
