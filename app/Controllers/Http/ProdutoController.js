'use strict'

const Produto = use('App/Models/Produto')

class ProdutoController {

  async index () {

    const produto = Produto.all()
    
    return produto
  }

  async store ({ request, auth }) {
    const id = auth.user.id

    const data = request.only(['prodescricao','vlrcusto', 'vlrvenda', 'user_id'])

    const produto = await Produto.create({...data, user_id: id})

    return produto
  }

  async show ({ params }) {
    const produto = await Produto.findOrFail(params.id)

    return produto
  }

  async update ({ params, request, auth,response }) {
    const produto = await Produto.findOrFail(params.id)

    if(produto.user_id !== auth.user.id ){
      return response.status(401).send({error: 'Produto não localizado!'})
    }

    const data = request.only(['prodescricao','vlrcusto', 'vlrvenda'])

    produto.merge(data)

    await produto.save()

    return produto

  }

  async destroy ({ params, auth, response }) {
    const produto = await Produto.findOrFail(params.id)

    if (produto.user_id != auth.user.id){
      return response.status(401).send({Error: 'Não foi autorizado!'})

    }

    await produto.delete()
  }
}

module.exports = ProdutoController
